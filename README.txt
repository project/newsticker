Newsticker provides the ability to add and manage newsticker items consisting of a Title, Link, and Weight.  You can choose from several transition effects and speeds and the ticker can be displayed in a block.

Big thanks to Mike Alsup for the JCycle library (http://malsup.com/jquery/cycle/) and the URL validation from the link module.

Installing Newsticker:
  Download the Newsticker release for your version.
  Untar it in the modules directory.
  Activate the module through drupals administrative interface.
  
Requires a new version of JQuery, so jquery_update is required.