<?php

define('LINK_EXTERNAL', 'external');
define('LINK_INTERNAL', 'internal');
define('LINK_FRONT', 'front');
define('LINK_EMAIL', 'email');
define('LINK_DOMAINS', 'aero|arpa|biz|com|cat|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|mobi');

/**
 * Implementation of hook_perm().
 */
function newsticker_perm() {
  return array('create newsticker item', 'edit newsticker item', 'edit own newsticker item', 'administer newsticker');
}

function newsticker_menu($may_cache){
  $items = array();

  if($may_cache){
    $items[] = array(
      'path' => 'admin/build/newsticker',
      'title' => t('Newsticker'),
      'callback' => 'newsticker_items_list',
      'access' => user_access('edit newsticker item') || user_access('edit own newsticker item') || user_access('create newsticker item') ,
    );
    $items[] = array('path' => 'admin/build/newsticker/list', 'title' => t('List'),
      'type' => MENU_DEFAULT_LOCAL_TASK, 'weight' => -10);    
    $items[] = array(
      'path' => 'admin/build/newsticker/add',
      'title' => t('Add Newsticker Item'),
      'callback' => 'newsticker_item_edit',
      'access' => user_access('create newsticker item'),
      'type' => MENU_LOCAL_TASK,
    );
    $items[] = array(
      'path' => 'admin/build/newsticker/settings',
      'title' => t('Newsticker Settings'),
      'description' => t('Adjust settings for the Newsticker.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('newsticker_settings'),
      'access' => user_access('administer newsticker'),
      'type' => MENU_LOCAL_TASK,
    );    
  }
  else{
    $items[] = array(
      'path' => 'newsticker/'. arg(1) .'/edit', 
      'title' => t('Edit'),
      'callback' => 'newsticker_item_edit',
      'callback arguments' => array(arg(1)),
      'access' => user_access('edit newsticker item') || user_access('edit own newsticker item'),
      'type' => MENU_CALLBACK);
    $items[] = array(
      'path' => 'newsticker/'. arg(1) .'/delete', 
      'title' => t('Delete'),
      'callback' => 'newsticker_item_delete',
      'callback arguments' => array(arg(1)),
      'access' => user_access('edit newsticker item') || user_access('edit own newsticker item'),
      'type' => MENU_CALLBACK);    
  }

  return $items;  
}


function newsticker_settings(){
  $options = array('fade' => t('fade'), 'shuffle' => t('shuffle'), 'zoom' => t('zoom'), 'slideX' => t('slideX'), 'slideY' => t('slideY'), 'scrollUp' => t('scrollUp'), 'scrollDown' => t('scrollDown'), 'scrollLeft' => t('scrollLeft'), 'scrollRight' => t('scrollRight'));
  $form['newsticker_effect'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Transition Effect'),
    '#default_value' => variable_get('newsticker_effect', 'fade'),
  );
  $form['newsticker_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the speed of the rotator'),
    '#default_value' => variable_get('newsticker_speed', 4000),
    '#description' => t('Milliseconds between slide transitions (0 to disable auto advance).'),
  );  
  $form['newsticker_pause'] = array(
    '#type' => 'checkbox',
    '#title' => t('Pause on hover'),
    '#default_value' => variable_get('newsticker_pause', 1),
    '#description' => t('True to pause when hovering over Newsticker.'),
  ); 
  return system_settings_form($form);  
}


function newsticker_item_edit($ntid = NULL) {
  return drupal_get_form('newsticker_item_form', $ntid);
}

function newsticker_item_form($ntid = NULL){
  $form = array();
  $item = $ntid ? db_fetch_array(db_query('SELECT * FROM {newsticker_item} WHERE ntid = %d', $ntid)) : NULL;
  
  $form['title'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Title'),
    '#required'      => TRUE,
    '#default_value' => $item['title'],
  );
  $form['link'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Link'),
    '#required'      => FALSE,
    '#default_value' => $item['link'],
  );    
  $form['weight'] = array(
    '#title' => t('Weight'),
    '#type' => 'weight', 
    '#default_value' => $item['weight'],
  );  
  
  if ($item) { 
    $form['ntid'] = array(
      '#type'            => 'hidden', 
      '#default_value'   => $item['ntid'],
    ); 
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Item'),
  );

  
  return $form;
}

function newsticker_item_form_validate($form_id, $form_values){
  if(trim($form_values['link']) != '' && !newsticker_validate_url($form_values['link']))
    form_set_error('link', t('Please ensure you entered a valid link.'));
}

function newsticker_item_form_submit($form_id, $form_values){
  $ntid = $form_values['ntid'] ? $form_values['ntid'] : NULL;
  $link = newsticker_cleanup_url($form_values['link']);
  if(!$ntid){
    $result = db_query("INSERT INTO {newsticker_item} (ntid, title, link, weight) VALUES (%d, '%s', '%s', %d)", db_next_id('{newsticker_item}_ntid'), $form_values['title'], $link, $form_values['weight']);
    $msg = 'Newsticker item created.';
  }
  else{
    $result = db_query("UPDATE {newsticker_item} set title = '%s', link = '%s', weight = %d WHERE ntid = %d", $form_values['title'], $link, $form_values['weight'], $ntid);
    $msg = 'Newsticker item updated.';
  }
  
  if($result){
    drupal_set_message($msg);
    drupal_goto('admin/build/newsticker');
  }
}


function newsticker_items_list(){
  $result = db_query('SELECT ntid, weight, title, link FROM {newsticker_item} ORDER BY weight');
  if($result){
    $headers = array(
      array('data' => t('Weight')),
      array('data' => t('Title')),
      array('data' => t('Link')),
      array('data' => t('Operations')),
    );
    
    $rows = array();
    while($item = db_fetch_array($result)){
      $rows[] = array($item['weight'], $item['title'], l($item['link'], $item['link']), 
        l('edit', 'newsticker/'.$item['ntid'].'/edit') . ' | ' . l('delete', 'newsticker/'.$item['ntid'].'/delete'));
    }
    
    $output = theme('table', $headers, $rows, array('id' => 'newsticker_item_list'));
   }
  
  return $output;
}

function newsticker_item_delete($ntid){
  $result = db_query('DELETE FROM {newsticker_item} WHERE ntid = %d', $ntid);
  drupal_set_message('Newsticker item deleted.');
  drupal_goto('admin/build/newsticker');
}

function newsticker_block($op = 'list', $delta = 0, $edit = array()) {
  switch($op){
    case 'list':
      $blocks[0] = array(
        'info' => t('Newsticker Items'),
      );
      return $blocks;
      break;
    case 'view':
      $block = array(
        'content' => newsticker_get_rotator()
      );
      return $block;
      break;
  }
}

function newsticker_get_rotator(){
  $result = db_query('SELECT title, link FROM {newsticker_item} ORDER BY weight');

  if($result){
    drupal_add_js(drupal_get_path('module', 'newsticker') . '/jcycle.js');
    drupal_add_js("$(function() { $('div#newsticker').cycle({fx: '". variable_get('newsticker_effect', 'fade') ."', timeout: ". variable_get('newsticker_speed', 4000) .", pause: ". variable_get('newsticker_pause', 1) ."}); });", 'inline');
    
    $output = '';
    while ($item = db_fetch_array($result)) {
      if(trim($item['link']) != '' )
        $content = l($item['title'], $item['link']);
      else
        $content = $item['title'];
        
      $output .= '<div class="newsticker-item">' . $content . '</div>';
    }
    return '<div id="newsticker">' . $output . '</div>';
  }
}

/**
 * Forms a valid URL if possible from an entered address.
 * Trims whitespace and automatically adds an http:// to addresses without a protocol specified
 *
 * @param string $url
 * @param string $protocol The protocol to be prepended to the url if one is not specified
 */
function newsticker_cleanup_url($url, $protocol = "http") {
  $url = trim($url);
  $type = newsticker_validate_url($url);
    
  if ($type == LINK_EXTERNAL) {
    // Check if there is no protocol specified
    $protocol_match = preg_match("/^([a-z0-9][a-z0-9\.\-_]*:\/\/)/i",$url);
    if (empty($protocol_match)) {
      // But should there be? Add an automatic http:// if it starts with a domain name
      $domain_match = preg_match('/^(([a-z0-9]([a-z0-9\-_]*\.)+)('. LINK_DOMAINS .'|[a-z]{2}))/i',$url);
      if (!empty($domain_match)) {
        $url = $protocol."://".$url;
      }
    }
  }
  
  return $url;
}

function newsticker_validate_url($text) {
  
  $allowed_protocols = variable_get('filter_allowed_protocols', array('http', 'https', 'ftp', 'news', 'nntp', 'telnet', 'mailto', 'irc', 'ssh', 'sftp', 'webcal'));
  
  $protocol = '((' . implode("|", $allowed_protocols) . '):\/\/)';
  $authentication = '([a-z0-9]+(:[a-z0-9]+)?@)';
  $domain = '(([a-z0-9]([a-z0-9\-_\[\]]*\.)+)('. LINK_DOMAINS .'|[a-z]{2}))';
  $ipv4 = '([0-9]{1,3}(\.[0-9]{1,3}){3})'; 
  $ipv6 = '([0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7})'; 
  $port = '(:([0-9]{1,5}))';
  
  // Pattern specific to eternal links
  $external_pattern = '/^' . $protocol . '?'. $authentication . '?' . '(' . $domain . '|' . $ipv4 . '|' . $ipv6 . ' |localhost)' . $port . '?';
  
  // Pattern specific to internal links
  $internal_pattern = "/^([a-z0-9_\-+\[\]]+)";
  
  $directories = "(\/[a-z0-9_\-\.~+%=&,$'():;*@\[\]]*)*";
  $query = "(\/?[?a-z0-9+_\-\.\/%=&,$'():;*@\[\]]*)";
  $anchor = "(#[a-z0-9_\-\.~+%=&,$'():;*@\[\]]*)";
  
  // the rest of the path for a standard URL
  $end = $directories . '?' . $query . '?' .  $anchor . '?' . '$/i';
  
  $user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\'\[\]]+';
  $email_pattern = '/^mailto:' . $user . '@' . '(' . $domain . '|' . $ipv4 .'|'. $ipv6 . '|localhost)' . $query . '$/';
  
  if (preg_match($external_pattern . $end, $text)) {
    return LINK_EXTERNAL;
  }
  elseif (preg_match($internal_pattern . $end, $text)) {
    return LINK_INTERNAL;
  }
  elseif (in_array('mailto', $allowed_protocols) && preg_match($email_pattern, $text)) {
    return LINK_EMAIL;
  }
  elseif (strpos($text, '<front>') === 0) {
    return LINK_FRONT;
  }
  return FALSE;
}
